FROM docker.elastic.co/logstash/logstash-oss:8.12.1
USER root
RUN rm -f /usr/share/logstash/pipeline/logstash.conf &&\
    apt-get install -y zip unzip &&\
    zip -q -d /usr/share/logstash/logstash-core/**/*/log4j-core-2.* org/apache/logging/log4j/core/lookup/JndiLookup.class
USER logstash
COPY log4j2.properties /usr/share/logstash/config/log4j2.properties
COPY java/java.security /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.161-0.b14.el7_4.x86_64/jre/lib/security/java.security
ADD pipeline/ /usr/share/logstash/pipeline/
